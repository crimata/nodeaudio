# nodeaudio

## Building

Clean and build:

    node-gyp rebuild

There is a custom, prebuilt PortAudio library (libportaudio.dylib) in portaudio/bin that is copied over to the rpath (build/Release) before the actual build. During build, it is dynamically linked to nodeaudio.node (the main binary).  This will NOT work (potentially at runtime) unless PortAudio has the correct install path. Run through the steps below if libportaudio.dylib changes:

Update the install path of PortAudio:

    install_name_tool -id "@loader_path/libportaudio.dylib" libportaudio.dylib

To see what libraries a Mach-O exe requires:

    otool -L build/Release/nodeAudio.node

## Publishing

The publishing config is defined in .npmrc (url and credentials).  API_TOKEN is a GitLab token with the api scope.

As of writing, publishing will fail if there is already a package in the registry.

### !! NPM vs Yarn
I was unable to figure out how to publish to the private GitLab repo using npm (400 Bad Request).  This may just be a bug in npm.  For real, this threw me for a loop.  I had to switch to Yarn for this reason (v1.22.17).  Pulling with npm works fine though (for other repos).

### TODO
Sign on build
