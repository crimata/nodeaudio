const nodeAudio = require('bindings')('nodeaudio.node');

let inputDevice;
let outputDevice;

const setStreams = () => {

    const defaultInput = nodeAudio.GetDefaultInputDevice();
    const defaultOutput = nodeAudio.GetDefaultOutputDevice();
    console.log(`Input: ${defaultInput}, Output: ${defaultOutput}`);

    if (inputDevice !== defaultInput) {
        inputDevice = defaultInput;
        nodeAudio.CloseInputStream(inputDevice);
        nodeAudio.OpenInputStream(inputDevice);
    }

    if (outputDevice !== defaultOutput) {
        outputDevice = defaultOutput;
        nodeAudio.CloseOutputStream(outputDevice);
        nodeAudio.OpenOutputStream(outputDevice);
    }
}

function x() {
    console.log("emitting");
}

nodeAudio.Initialize(x);

const id = setInterval(setStreams, 3000);

setTimeout(() => {
    clearInterval(id);
    nodeAudio.Terminate();
}, 11000);
