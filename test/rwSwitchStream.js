const nodeAudio = require('../index.js');
      EventEmitter = require('events'),
      emitter = new EventEmitter();

/* Set up keyboard input for recording */
var stdin = process.stdin;
stdin.setRawMode( true );
stdin.resume();
stdin.setEncoding( 'utf8' );

let recording = false;

let inputDevice;
let outputDevice;

const chunks = [];
emitter.on("data", (int16Arr) => {
    if (recording) {
        console.log(`Captured ${int16Arr.length} frames`);
        chunks.push(int16Arr);
    }
});

emitter.on("write", () => {
    console.log("Writing frames");
});

const setStreams = () => {

    const defaultInput = nodeAudio.core.GetDefaultInputDevice();
    const defaultOutput = nodeAudio.core.GetDefaultOutputDevice();
    console.log(`Input: ${defaultInput}, Output: ${defaultOutput}`);

    if (inputDevice !== defaultInput) {
        inputDevice = defaultInput;
        nodeAudio.core.CloseInputStream(inputDevice);
        nodeAudio.core.OpenInputStream(inputDevice);
    }

    if (outputDevice !== defaultOutput) {
        outputDevice = defaultOutput;
        nodeAudio.core.CloseOutputStream(outputDevice);
        nodeAudio.core.OpenOutputStream(outputDevice);
    }
}

const stopRecording = () => {
    recording = false;
    pcmAudio = nodeAudio.utils.mergeChunks(chunks);
    chunks.length = 0;

    /* Play recorded audio */
    console.log("Playing recorded audio...");
    nodeAudio.core.WriteToOutputStream(pcmAudio.buffer);

    /* Two writes test */
    // setTimeout(() => {
    //     console.log("Playing again...");
    //     nodeAudio.core.WriteToOutputStream(pcmAudio.buffer);
    // }, 2000);

    /* Cancel mid-playback test */
    setTimeout(() => {
        console.log("Cancelling...");
        nodeAudio.core.CancelPlayback();
    }, 2000);
}

nodeAudio.core.Initialize(emitter.emit.bind(emitter));

/* Check every 2 seconds for new audio device */
setInterval(setStreams, 2000);

/* Toggle recorder with 'r' */
stdin.on( 'data', function( key ) {
    if (key == "r") {
        if (recording) {
            stopRecording();
        } else {
            recording = true;
        }
    }
});

setTimeout(() => {
    nodeAudio.core.Terminate();
    process.exit();
}, 20000);