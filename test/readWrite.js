const nodeAudio = require('bindings')('nodeaudio.node'),
      EventEmitter = require('events'),
      sleep = require('sleep'),
      emitter = new EventEmitter();

let recording = false;

const chunks = [];
emitter.on("data", (int16Arr) => {
    if (recording) {
        console.log(`Captured ${int16Arr.length} frames`);
        chunks.push(int16Arr);
    }
});

nodeAudio.Initialize(emitter.emit.bind(emitter)); /* Accepts an event emitter */

nodeAudio.OpenInputStream(nodeAudio.GetDefaultInputDevice());
nodeAudio.OpenOutputStream(nodeAudio.GetDefaultOutputDevice());

recording = true;

setTimeout(() => {

    recording = false;
    pcmAudio = mergeChunks(chunks);
    chunks.lengh = 0;

    nodeAudio.WriteToOutputStream(pcmAudio.buffer);

    setTimeout(nodeAudio.Terminate, 5000);

}, 2000);

const mergeChunks = (chunks) => {
    const chunkLengths = chunks.map((b) => b.length), // as well as here... 
          totalChunkLength = chunkLengths.reduce((p, c) => p+c, 0),
          int16Arr = new Int16Array(totalChunkLength);

    console.log(`${totalChunkLength} frames captured in total.`);

    let offset = 0;
    for (var i = 0; i < chunks.length; i++) { // and here...
        int16Arr.set(chunks[i], offset); // seg fault happens here.
        offset += chunkLengths[i];
    }

    return int16Arr;
}

 // we can also set js-created TypedArrays in line 41.