{
  "targets": [
    {
      "target_name": "nodeAudio",
      "sources": [ 
        "src/na_front.c", 
        "src/na_core.c", 
        "src/na_callbacks.c", 
        "src/na_utils.c",
        "src/na_buffer.c",
        "src/pa_ringbuffer.c",
      ],
      "include_dirs": [
        "portaudio/include"
      ],
      "libraries": [
        "libportaudio.dylib"
      ],
      "copies": [
        {
          "destination": "build/Release/",
          "files": [
            "portaudio/bin/libportaudio.dylib"
          ]
        }
      ],
    }
  ]
}
