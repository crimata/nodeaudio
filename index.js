const nodeAudio = require('bindings')('nodeaudio.node');

/* Utility function that merges Int16Arrays */
const mergeChunks = (chunks) => {
    const chunkLengths = chunks.map((b) => b.length),
          totalChunkLength = chunkLengths.reduce((p, c) => p+c, 0),
          int16Arr = new Int16Array(totalChunkLength);

    console.log(`${totalChunkLength} frames captured in total.`);

    let offset = 0;
    for (var i = 0; i < chunks.length; i++) {
        int16Arr.set(chunks[i], offset);
        offset += chunkLengths[i];
    }

    return int16Arr;
}

const utils = { mergeChunks: mergeChunks };

exports.core = nodeAudio;
exports.utils = utils;
