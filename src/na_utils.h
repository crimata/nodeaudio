#ifndef NA_UTILS
#define NA_UTILS

#include "pa_ringbuffer.h"

double setParameters( int type,
                      PaDeviceIndex deviceIndex,
                      PaStreamParameters* parameters );

ring_buffer_size_t rbs_min(ring_buffer_size_t a, ring_buffer_size_t b);

#endif