#include <stdio.h>
#include <portaudio.h>

#include "na_utils.h"

double setParameters( int type,
                      PaDeviceIndex deviceIndex,
                      PaStreamParameters* parameters )
{
    const PaDeviceInfo* deviceInfo;
    printf("Setting parameters for new stream:\n");

    deviceInfo = Pa_GetDeviceInfo( deviceIndex );

    parameters->device = deviceIndex;
    if ( type == 0 )
    {
        printf("    Type: input\n");
        parameters->channelCount = 1;
        printf("    Channels: %i\n", parameters->channelCount);
    }
    else 
    {
        printf("    Type: output\n");
        parameters->channelCount = 1;
        printf("    Channels: %i\n", parameters->channelCount);
    }
    parameters->sampleFormat = paInt16;
    parameters->hostApiSpecificStreamInfo = NULL;
    parameters->suggestedLatency = deviceInfo->defaultHighInputLatency;

    printf("    Device: %s\n", deviceInfo->name);
    // printf("    SampleRate: %i\n", (int) deviceInfo->defaultSampleRate);
    printf("    SampleRate: %i\n", 16000);

    /* return deviceInfo->defaultSampleRate; */
    return 16000;
}

ring_buffer_size_t rbs_min(ring_buffer_size_t a, ring_buffer_size_t b)
{
    return (a < b) ? a : b;
}