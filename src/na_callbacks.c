#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <node_api.h>
#include <portaudio.h>

#include "na_utils.h"
#include "na_callbacks.h"
#include "pa_ringbuffer.h"

/* JS callbacks */
napi_threadsafe_function js_on_data;
napi_threadsafe_function js_on_playback;

/* Calls js_emit_ts from the main thread and translates data to node */
static void call_js_on_data( napi_env   env,
                             napi_value on_data,
                             void*      context, /* the inputBuffer */
                             void*      data )
{
    (void) data;
    napi_status status;

    napi_value channel;
    status = napi_create_string_utf8(env, "data", NAPI_AUTO_LENGTH, &channel);

    PaUtilRingBuffer* rb = (PaUtilRingBuffer*) context;

    ring_buffer_size_t framesToRead;
    framesToRead = PaUtil_GetRingBufferReadAvailable(rb);

    void* frames; /* read the data into here */
    frames = malloc(framesToRead * 2);
    PaUtil_ReadRingBuffer(rb, frames, framesToRead);

    napi_value arrayBuffer;
    napi_create_external_arraybuffer( env,
                                      frames,
                                      framesToRead * 2,
                                      NULL,
                                      NULL,
                                      &arrayBuffer );

    napi_value typedArray;
    status = napi_create_typedarray( env,
                                     napi_int16_array,
                                     framesToRead,
                                     arrayBuffer,
                                     0,
                                     &typedArray );

    napi_value undefined;
    status = napi_get_undefined(env, &undefined);

    size_t argc = 2;
    napi_value args[2] = { channel, typedArray };
    status = napi_call_function(env, undefined, on_data, argc, &args[0], NULL);
}

static void call_js_on_playback( napi_env   env,
                                 napi_value on_data,
                                 void*      context,
                                 void*      data )
{
    (void) context;
    napi_status status;

    napi_value channel;
    status = napi_create_string_utf8(env, "write", NAPI_AUTO_LENGTH, &channel);

    napi_value undefined;
    status = napi_get_undefined(env, &undefined);

    size_t argc = 1;
    napi_value args[1] = { channel };
    status = napi_call_function(env, undefined, on_data, argc, &args[0], NULL);
}

/* Takes in a Node Event Emitter and creates JS threadsafe functions to be 
 * called from the PortAudio callbacks.
 */
void init_callbacks( napi_env          env,
                     napi_value*       js_emit,
                     PaUtilRingBuffer* inputBuffer )
{    
    napi_status status;
    napi_value resource_name;
    status = napi_create_string_utf8( env,
                                      "Thread Safe Event Emitter",
                                      NAPI_AUTO_LENGTH,
                                      &resource_name );

    /* Create thread-safe js_on_data callback */
    status = napi_create_threadsafe_function( env,
                                              *js_emit,
                                              NULL,
                                              resource_name,
                                              0,
                                              1,
                                              NULL,
                                              NULL, 
                                              inputBuffer, /* context */
                                              call_js_on_data,
                                              &js_on_data );

    /* Create thread-safe js_on_write callback */
    status = napi_create_threadsafe_function( env,
                                              *js_emit,
                                              NULL,
                                              resource_name,
                                              0,
                                              1,
                                              NULL,
                                              NULL, 
                                              inputBuffer, /* context */
                                              call_js_on_playback,
                                              &js_on_playback );
}

int input_callback( const void*                     input, 
                    void*                           output, 
                    unsigned long                   frameCount, 
                    const PaStreamCallbackTimeInfo* timeInfo,
                    PaStreamCallbackFlags           statusFlags, 
                    void*                           data )
{
    (void) output;

    PaUtilRingBuffer* rb = (PaUtilRingBuffer*) data;

    ring_buffer_size_t writableSpace;
    writableSpace = PaUtil_GetRingBufferWriteAvailable(rb);

    ring_buffer_size_t framesToWrite;
    framesToWrite = rbs_min(writableSpace, frameCount);

    PaUtil_WriteRingBuffer(rb, input, framesToWrite);

    napi_status status;
    status = napi_call_threadsafe_function( js_on_data,
                                            NULL,
                                            napi_tsfn_nonblocking );

    return paContinue;
}


int output_callback( const void*                     input, 
                     void*                           output, 
                     unsigned long                   frameCount, 
                     const PaStreamCallbackTimeInfo* timeInfo,
                     PaStreamCallbackFlags           statusFlags, 
                     void*                           data )
{
    (void) input;

    /* Reset output data first */
    memset(output, 0, frameCount * 2);

    PaUtilRingBuffer* rb = (PaUtilRingBuffer*) data;

    ring_buffer_size_t framesInQueue;
    framesInQueue = PaUtil_GetRingBufferReadAvailable(rb);

    ring_buffer_size_t framesToRead;
    framesToRead = rbs_min(framesInQueue, (ring_buffer_size_t) frameCount);

    if (framesToRead != 0)
    {
        PaUtil_ReadRingBuffer(rb, output, framesToRead);

        napi_status status;
        status = napi_call_threadsafe_function( js_on_playback,
                                                NULL,
                                                napi_tsfn_nonblocking );
    }

    return paContinue;
}

// short* sample = (short*) output;
// for (int i = 0; i < framesToRead * 2; ++i)
// {
//     printf("%hu", sample[i]);
// }
