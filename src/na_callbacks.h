#ifndef NA_CALLBACKS
#define NA_CALLBACKS

typedef struct
{
    size_t len; /* in frames */
    const void* data;
} 
audio_info;

void init_callbacks( napi_env          env,
                     napi_value*       js_emit,
                     PaUtilRingBuffer* inputBuffer );

int input_callback( const void*                     input, 
                    void*                           output, 
                    unsigned long                   frameCount, 
                    const PaStreamCallbackTimeInfo* timeInfo,
                    PaStreamCallbackFlags           statusFlags, 
                    void*                           data );

int output_callback( const void*                     input, 
                     void*                           output, 
                     unsigned long                   frameCount, 
                     const PaStreamCallbackTimeInfo* timeInfo,
                     PaStreamCallbackFlags           statusFlags, 
                     void*                           data );

#endif