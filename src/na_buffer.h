#ifndef NA_BUFFER
#define NA_BUFFER

void init_buffer(PaUtilRingBuffer* buffer, void** store);

void free_buffer(PaUtilRingBuffer* buffer);

#endif