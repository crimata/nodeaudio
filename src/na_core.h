#ifndef NA_CORE
#define NA_CORE

typedef struct
{
    size_t len;
    void* data;
}
playback_data;

PaError Na_Initialize(napi_env env, napi_value* js_emit);

PaError Na_GetDefaultInputDevice(PaDeviceIndex* deviceIndex);

PaError Na_GetDefaultOutputDevice(PaDeviceIndex* deviceIndex);

PaError Na_OpenInputStream(int deviceIndex);

PaError Na_OpenOutputStream(int deviceIndex);

PaError Na_CloseInputStream();

PaError Na_CloseOutputStream();

void Na_WriteToOutputBuffer(void* data, size_t size);

void Na_CancelPlayback();

PaError Na_Terminate();

#endif