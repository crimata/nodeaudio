#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include <node_api.h>
#include <portaudio.h>

#include "na_core.h"
#include "na_utils.h"
#include "na_buffer.h"
#include "na_callbacks.h"
#include "pa_ringbuffer.h"

PaStream* inputStream;
PaStream* outputStream;

PaUtilRingBuffer inputBuffer;
PaUtilRingBuffer outputBuffer;
void* inputBufferStore;
void* outputBufferStore;

pthread_t pb_th;
pthread_mutex_t mutex;

PaError Na_Initialize(napi_env env, napi_value* js_emit)
{
    init_callbacks(env, js_emit, &inputBuffer);

    pthread_mutex_init(&mutex, NULL);

    PaError err;
    err = Pa_Initialize();
    return err;
}

PaError Na_GetDefaultInputDevice(PaDeviceIndex* deviceIndex)
{
    PaError err;
    err = Pa_RefreshDevices();
    if (err != paNoError) return err;

    *deviceIndex = Pa_GetDefaultInputDevice();
    return err;
}

PaError Na_GetDefaultOutputDevice(PaDeviceIndex* deviceIndex)
{
    PaError err;
    err = Pa_RefreshDevices();
    if (err != paNoError) return err;

    *deviceIndex = Pa_GetDefaultOutputDevice();
    return err;
}

PaError Na_OpenInputStream(int deviceIndex)
{    
    PaError err;
    if ((err = Pa_IsStreamActive(inputStream) == 1))
    {
        err = paDeviceUnavailable;
    }
    if (err != paNoError) return err;

    init_buffer(&inputBuffer, &inputBufferStore);

    double sampleRate;
    PaStreamParameters parameters;
    sampleRate = setParameters(0, (PaDeviceIndex) deviceIndex, &parameters);

    err = Pa_OpenStream( &inputStream,
                         &parameters,
                         NULL,
                         sampleRate,
                         paFramesPerBufferUnspecified,
                         paClipOff,
                         input_callback,
                         &inputBuffer );
    if (err != paNoError) return err;

    printf("NA_CORE:Na_OpenInputStream:starting stream...\n");
    err = Pa_StartStream(inputStream);
    return err;
}

PaError Na_OpenOutputStream(int deviceIndex)
{
    PaError err;
    if ((err = Pa_IsStreamActive(outputStream) == 1))
    {
        err = paDeviceUnavailable;
    }
    if (err != paNoError) return err;

    init_buffer(&outputBuffer, &outputBufferStore);

    double sampleRate;
    PaStreamParameters parameters;
    sampleRate = setParameters(1, (PaDeviceIndex) deviceIndex, &parameters);

    err = Pa_OpenStream( &outputStream,
                         NULL,
                         &parameters,
                         sampleRate,
                         paFramesPerBufferUnspecified,
                         paClipOff,
                         output_callback,
                         &outputBuffer ); /* pass in outputBuffer here */
    if (err != paNoError) return err;

    printf("NA_CORE:Na_OpenOutputStream:starting stream...\n");
    err = Pa_StartStream(outputStream);
    return err;
}

PaError Na_CloseInputStream()
{
    PaError err = paNoError;
    PaStreamInfo* info;
    if ((info = Pa_GetStreamInfo(inputStream) != NULL))
    {
        err = Pa_CloseStream(inputStream);
    }
    return err;
}

PaError Na_CloseOutputStream() 
{
    PaError err = paNoError;
    PaStreamInfo* info;
    if ((info = Pa_GetStreamInfo(outputStream) != NULL))
    {
        err = Pa_CloseStream(outputStream);
    }
    return err;
}

bool cancel_pb_th = false;

static void WriteLargeDataToOutputBuffer(void* arg)
{
    playback_data* pd = (playback_data*) arg;

    ring_buffer_size_t writableSpace;
    ring_buffer_size_t framesToWrite;
    ring_buffer_size_t framesWritten;

    pthread_mutex_lock(&mutex);
    cancel_pb_th = false;
    while (pd->len > 0 && !cancel_pb_th)
    {
        writableSpace = PaUtil_GetRingBufferWriteAvailable(&outputBuffer);
        if (writableSpace != 0)
        {
            framesToWrite = rbs_min( writableSpace, 
                                     (ring_buffer_size_t) pd->len );

            framesWritten = PaUtil_WriteRingBuffer( &outputBuffer, 
                                                    pd->data, 
                                                    framesToWrite );

            pd->len -= framesWritten;
            pd->data += framesWritten * 2; /* convert to bytes */
        }
    }
    pthread_mutex_unlock(&mutex);

    free(arg);
}

void Na_WriteToOutputBuffer(void* data, size_t size)
{
    ring_buffer_size_t writableSpace;
    writableSpace = PaUtil_GetRingBufferWriteAvailable(&outputBuffer);

    /* If there is not enough space to write all the data at once, we must
     * start a new thread in order to prevent blocking.
     */
    size_t len = size / 2;
    if ((size_t) writableSpace < len)
    {
        playback_data* pd = (playback_data*) malloc(sizeof(playback_data));
        pd->data = data;
        pd->len = len;
        pthread_create(&pb_th, NULL, (void*) WriteLargeDataToOutputBuffer, pd);
    }
    else
    {
        pthread_mutex_lock(&mutex);
        PaUtil_WriteRingBuffer(&outputBuffer, data, writableSpace);
        pthread_mutex_unlock(&mutex);
    }
}

void Na_CancelPlayback()
{
    printf("NACORE:Na_CancelPlayback\n");
    cancel_pb_th = true;
}

PaError Na_Terminate()
{
    printf("NACORE:Na_Terminate\n");

    PaError err;
    if ((err = Pa_IsStreamActive(inputStream) == 1))
    {
        err = Pa_AbortStream(inputStream);
    }
    if ((err = Pa_IsStreamActive(outputStream) == 1))
    {
        err = Pa_AbortStream(outputStream);
    }
    err = Pa_Terminate();

    free_buffer(&inputBuffer);
    free_buffer(&outputBuffer);

    pthread_mutex_destroy(&mutex);

    return err;
}



