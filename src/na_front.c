#include <stdio.h>
#include <assert.h>
#include <pthread.h>
#include <node_api.h>
#include <portaudio.h>

#include "na_core.h"

#define DECLARE_NAPI_METHOD(name, func) { name, 0, func, 0, 0, 0, napi_default, 0 }

napi_value Initialize(napi_env env, napi_callback_info info) 
{
    printf("NAPI::Initialize\n");
    
    size_t argc = 1;
    napi_value args[1];
    napi_status status;
    status = napi_get_cb_info(env, info, &argc, args, NULL, NULL);

    PaError err;
    napi_value js_cb = args[0];
    err = Na_Initialize(env, &js_cb);
    if (err != paNoError)
    {
        napi_throw_error(env, NULL, Pa_GetErrorText(err));
    }

    return NULL;
}

napi_value GetDefaultInputDevice(napi_env env, napi_callback_info info)
{
    printf("NAPI::GetDefaultInputDevice\n");
    (void) info;

    int deviceIndex;
    PaError err;
    err = Na_GetDefaultInputDevice(&deviceIndex);
    if (err != paNoError)
    {
        napi_throw_error(env, NULL, Pa_GetErrorText(err));
    }

    napi_status status;
    napi_value js_deviceIndex;
    status = napi_create_int32(env, deviceIndex, &js_deviceIndex);

    return js_deviceIndex;
}

napi_value GetDefaultOutputDevice(napi_env env, napi_callback_info info)
{
    printf("NAPI::GetDefaultOutputDevice\n");
    (void) info;

    int deviceIndex;
    PaError err;
    err = Na_GetDefaultOutputDevice(&deviceIndex);
    if (err != paNoError)
    {
        napi_throw_error(env, NULL, Pa_GetErrorText(err));
    }

    napi_status status;
    napi_value js_deviceIndex;
    status = napi_create_int32(env, deviceIndex, &js_deviceIndex);

    return js_deviceIndex;
}

napi_value OpenInputStream(napi_env env, napi_callback_info info)
{
    printf("NAPI::OpenInputStream\n");

    size_t argc = 1; /* parse args */
    napi_value args[1];
    napi_status status;
    status = napi_get_cb_info(env, info, &argc, args, NULL, NULL);

    int deviceIndex;
    status = napi_get_value_int32( env,
                                   args[0],
                                   &deviceIndex );

    PaError err = paNoError;
    err = Na_OpenInputStream(deviceIndex);
    if (err != paNoError)
    {
        napi_throw_error(env, NULL, Pa_GetErrorText(err));
    }

    return NULL;
}

napi_value OpenOutputStream(napi_env env, napi_callback_info info)
{
    printf("NAPI::OpenOutputStream\n");

    size_t argc = 1; /* parse args */
    napi_value args[1];
    napi_status status;
    status = napi_get_cb_info(env, info, &argc, args, NULL, NULL);

    int deviceIndex;
    status = napi_get_value_int32( env,
                                   args[0],
                                   &deviceIndex );

    PaError err = paNoError;
    err = Na_OpenOutputStream(deviceIndex);
    if (err != paNoError)
    {
        napi_throw_error(env, NULL, Pa_GetErrorText(err));
    }

    return NULL;
}

napi_value CloseInputStream(napi_env env, napi_callback_info info)
{
    printf("NAPI::CloseInputStream\n");

    PaError err = paNoError;
    err = Na_CloseInputStream();
    if (err != paNoError)
    {
        napi_throw_error(env, NULL, Pa_GetErrorText(err));
    }

    return NULL;
}

napi_value CloseOutputStream(napi_env env, napi_callback_info info)
{
    printf("NAPI::CloseOutputStream\n");

    PaError err = paNoError;
    err = Na_CloseOutputStream();
    if (err != paNoError)
    {
        napi_throw_error(env, NULL, Pa_GetErrorText(err));
    }

    return NULL;
}

napi_value WriteToOutputStream(napi_env env, napi_callback_info info)
{
    printf("NAPI::WriteToOutputStream\n");

    size_t argc = 1; /* parse args */
    napi_value args[1];
    napi_status status;
    status = napi_get_cb_info(env, info, &argc, args, NULL, NULL);

    size_t size;
    void* data;
    status = napi_get_arraybuffer_info(env, args[0], &data, &size);

    Na_WriteToOutputBuffer(data, size);
    return NULL;
}

napi_value CancelPlayback(napi_env env, napi_callback_info info)
{
    printf("NAPI::CancelPlayback\n");
    (void) env;
    (void) info;

    Na_CancelPlayback();
    return NULL;
}

napi_value Terminate(napi_env env, napi_callback_info info)
{
    printf("NAPI::Terminate\n");
    (void) info;
    PaError err = paNoError;

    err = Na_Terminate();
    if (err != paNoError) 
    {
        napi_throw_error(env, NULL, Pa_GetErrorText(err));
    }

    return NULL;
}

napi_value Init(napi_env env, napi_value exports) 
{
  napi_status status;

  napi_property_descriptor desc[] = {
    DECLARE_NAPI_METHOD("Initialize", Initialize),
    DECLARE_NAPI_METHOD("Terminate", Terminate),
    DECLARE_NAPI_METHOD("GetDefaultInputDevice", GetDefaultInputDevice),
    DECLARE_NAPI_METHOD("GetDefaultOutputDevice", GetDefaultOutputDevice),
    DECLARE_NAPI_METHOD("OpenInputStream", OpenInputStream),
    DECLARE_NAPI_METHOD("OpenOutputStream", OpenOutputStream),
    DECLARE_NAPI_METHOD("CloseInputStream", CloseInputStream),
    DECLARE_NAPI_METHOD("CloseOutputStream", CloseOutputStream),
    DECLARE_NAPI_METHOD("WriteToOutputStream", WriteToOutputStream),
    DECLARE_NAPI_METHOD("CancelPlayback", CancelPlayback)
  };

  status = napi_define_properties(env, exports, 10, desc);

  return exports;

}


NAPI_MODULE(NODE_GYP_MODULE_NAME, Init)

