#include <stdlib.h>

#include "pa_ringbuffer.h"

void init_buffer(PaUtilRingBuffer* buffer, void** store)
{
    *store = (void*) malloc(2 * 8192);
    PaUtil_InitializeRingBuffer(buffer, 2, 8192, *store);
}

void free_buffer(PaUtilRingBuffer* buffer)
{
    free(buffer->buffer);
}